//
//  ViewController.swift
//  Multilanguage Adjustable
//
//  Created by Dawid Herman on 19/01/2022.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var languageButton: UIButton!
    @IBOutlet weak var sliderLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    var selectedLanguageCode = "en"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateTranslations()
    }
    
    func updateTranslations() {
        languageLabel.text = localizeString("label")
        languageButton.setTitle(localizeString("button"), for: .normal)
        sliderLabel.text = localizeString("label2")
        slider.accessibilityLanguage = selectedLanguageCode
        slider.accessibilityLabel = localizeString("slider")
        slider.accessibilityValue = localizeString("value: ") + String(slider.value)
    }

    func localizeString(_ key: String) -> String {
        if let path = Bundle.main.path(forResource: selectedLanguageCode, ofType: "lproj") {
            let languageBundle = Bundle(path: path)
            return languageBundle!.localizedString(forKey: key, value: "", table: nil)
        }
        else {
            return NSLocalizedString(key, comment: "")
        }
    }
    
    @IBAction func changeLanguage() {
        selectedLanguageCode = (selectedLanguageCode == "de") ? "en" : "de"
        updateTranslations()
    }
}

